```
ipamir.h          Reentrant Incremental MaxSAT solver API (reverse)
ipasir.h          Reentrant Incremental SAT solver API (reverse)

app               application directory
maxsat            MaxSAT solver directory
sat               SAT solver directory

app/README        explains the apps directory
maxsat/README     explains the MaxSAT solver directory
sat/README        explains the SAT solver directory

LICENSE           generic license for parts of this software not
                  explicitly covered by its own license restrictions,
                  which can be found in the corresponding LICENSE or
                  COPYRIGHT file in a sub directory or in a distribution
                  package, such as in an included tar or zip file
```
